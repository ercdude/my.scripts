#!/bin/sh
#
# [CHROOT SCRIPT] - Bootstrap, mounts and access chrooted environments
#
# This script works with a fstab.[name]. So if you want to mount a \"debian\"
# chroot, it's root path should be ${JAILS_PATH}/debian and a file
# /etc/fstab.debian should exists as the following:
#
# # Device   Mountpoint                     FStype       Options        Dump  Pass
# linprocfs  ${JAILS_PATH}/debian/proc      linprocfs    rw             0       0
# linsysfs   ${JAILS_PATH}/debian/sys       linsysfs     rw             0       0
# tmpfs      ${JAILS_PATH}/debian/tmp       tmpfs        rw,mode=1777   0       0
# /dev/pts/  ${JAILS_PATH}/debian/dev/pts   nullfs       rw             0       0
#

# The root where to search jails. Important not to add "/" at the end
JAILS_PATH=/jails
FSTAB_PATH=${JAILS_PATH}/etc

# This is a path that you want to mount inside chroot for accessing external files.
EXTERN_PATH=`readlink -f ~/.jails/shared`

# This is where the EXTERN_PATH will be mounted INSIDE chroot.
# If not existent, it will be created. (remember to add '/' at beggining)
#
# PS: For flow dev, the extern src should be mounted with the same user/permissions.
#     So cannot be pointed to a root directory. (TODO: are you sure?)
EXTERN_MNT=/extern

# bootstrap options
SUITE=stretch
VARIANT=buildd # Test with fakechroot

# Used when running a command. If not specified, mount/umount will not be called.
SHOULD_MOUNT=false
SHOULD_UMOUNT=false

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color

VERBOSE=true

print_help()
{
    ( is_freebsd ) && [ -z "${1}" ] \
        DISCLAIMER="${YELLOW}Make sure to have linux64 kernel module installed. It's needed when
mounting the environment. ${NC}"

    # If called with an arg, show it as DISCLAIMER (error?)
    [ -n "${1}" ] && DISCLAIMER=${RED}${1}${NC}

    echo -e "
Can't stay at heaven forever...

Usage: my.chroot {name} {command} [options]

Commands:
      bootstrap [opt]    Bootstrap a system (only debian for now).
                         This is still a WIP, the debootstrap isn't fully
                         supported by freebsd so it's installed with
                         --foreign option and additional config is needed
                         at first run to get apt-get and some basic stuff
                         working.

      mount              Mounts the linux environment.

      umount             Unmounts the linux environment.

      list               Lists what is mounted or not.
                         [ not implemented ]

      boot               Mount and, if succeeds, starts a shell.

      run {cmd} [opt]    Executes the command inside chroot. By default,
                         this mode set -v false

Options:
      -s suite           Sets the Debian Suite to _suite_

      -v variant         Sets the Debian Variant to _variant_
                         See man debootstrap (8) for details.

      -m                 Mount the chroot if not mounted.

      -u                 Unmount the chroot after execution.

      -e                 Exec only. Mount the chroot if it's not mounted. If
                         it's already mounted, it will try to run without
                         mounting.

      -p parameters      When running a command, you may pass parameters using
                         -p option. It helps with scripts.

      -V [true|false]    Toggle verbosity. Only warns and errors shown.

      -x                 Activate debug mode (set -x).

${GREEN}
--------------------------------------------------------------------------------
${NC}
${DISCLAIMER}
"
}

ERROR=error
WARN=warn

log()
{
    PREFIX="[-] "
    if [ -n "${2}" ]; then
        case ${1} in
            ${ERROR})
                PREFIX="${RED}[!]${NC} "
                break;;
            ${WARN})
                PREFIX="${YELLOW}[?]${NC} "
                break;;
            *)
                PREFIX="[-] "
                ;;
        esac
    shift
    elif [ "$VERBOSE" = false ]; then
        # skip for info when verbose is off
        return
    fi

    echo -e "${PREFIX} ${1}"
}

bootstrap()
{
    # Check (and create) fstab path
    if ! [ -d ${FSTAB_PATH} ]; then
        log "Creating path for fstab files: ${FSTAB_PATH}"
        ( mkdir -p ${FSTAB_PATH} ) || (
            log ${ERROR} "Could not create fstab path." &&
            return 1
        )
    fi

    log "Creating fstab file..."

    FSTAB_FILE=${FSTAB_PATH}/fstab.${NAME}
    if is_freebsd ; then
        # Create fstab for new chroot (freebsd)
        echo \
            "# Device     Mountpoint                    FStype       Options       Dump  Pass
            tmpfs        ${JAILS_PATH}/${NAME}/tmp     tmpfs        rw,mode=1777  0       0
            linprocfs    ${JAILS_PATH}/${NAME}/proc    linprocfs    rw            0       0
            linsysfs     ${JAILS_PATH}/${NAME}/sys     linsysfs     rw            0       0
            devfs        ${JAILS_PATH}/${NAME}/dev     nullfs       rw            0       0
            /dev/pts/    ${JAILS_PATH}/${NAME}/dev/pts nullfs       rw            0       0" > ${FSTAB_FILE}

    else
        # Create fstab for new chroot (linux)
        # DISCLAIMER: NOT TESTED
        echo \
            "# Device     Mountpoint                      FStype       Options                Dump    Pass
            /tmp         ${JAILS_PATH}/${NAME}/tmp       none         bind                   0       0
            proc         ${JAILS_PATH}/${NAME}/proc      proc         nosuid,noexec,nodev    0       0
            sysfs        ${JAILS_PATH}/${NAME}/sys       sysfs        nosuid,noexec,nodev    0       0
            devtmpfs     ${JAILS_PATH}/${NAME}/dev       devtmpfs     mode=0755,nosuid       0       0
            devpts       ${JAILS_PATH}/${NAME}/dev/pts   devpts       gid=5,mode=620         0       0" \
            > ${FSTAB_FILE}
    fi

    ( [ -s ${FSTAB_FILE} ] && log "fstab created: ${FSTAB_FILE}." ) || {
        log ${ERROR} "Could not create ${FSTAB_FILE}! What about permissions?"

        return 1
    }
    log "${YELLOW}If you want access outside of chroot, add the mountpoint wanted to the jail's fstab file:${NC}"
    log "/my/external/path     ${JAILS_PATH}/${NAME}/mount/point      nullfs     rw        0    0"

    echo
    log "Running bootstrap: debootstrap --foreign --variant=${VARIANT} ${SUITE} ${JAILS_PATH}/${NAME}"
    ( debootstrap --foreign --arch=amd64 \
                  --variant=${VARIANT} ${SUITE} ${JAILS_PATH}/${NAME} ) || {
        log ${ERROR} "Bootstrap failed... :/"
        return 1
    }

    # Create missing dir
    # TODO: check it before? log it?
    mkdir -p ${JAILS_PATH}/${NAME}/dev/pts

    log "${GREEN}DONE${NC}"
    log ${WARN} "Remember to initialize the basic packages!"
    log ${WARN} "${YELLOW}This chroot is not complete yet, now it's your job.${NC}"
    log "You may run the chroot and run:"
    log "  1) dpkg --force-depends -i /var/cache/apt/archives/*.deb"
    log "  2) dpkg --configure --pending"
    log "  3) export LC_ALL=C && dpkg-reconfigure locales"
    log "1 and 2 so you can use the basics packages (like apt-get)."
    log "3 to fix locales, otherwise you'll have problems with apt-get."

    # log "Moving on to setup packages..."

    # ( chroot ${JAILS_PATH}/${NAME} dpkg --force-depends -i /var/cache/apt/archives/*.deb ) || {
    #     log ${ERROR} "Failed to run dpkg... You are on your on now, but chroot can be mounted."
    #     return 1
    # }
    #
    # ( chroot ${JAILS_PATH}/${NAME} dpkg --configure --pending ) || {
    #     log ${ERROR} "Failed to configure dpkg... You are on your on now, but chroot can be mounted."
    #     return 1
    # }
    #
    # Change PS1 to show the name of chroot instead of the host:
    #     echo "PS1="\u@\[\033[1;32m\]\h\[\033[0m\]:\w\$" >> ${JAILS_PATH}/${NAME}/root/.bashrc
    #

    log "Done! You can mount the chroot now!"
    echo

    return 0
}

is_freebsd()
{
    # TODO
    return 0
}

load_linux_compat()
{
    # If not freebsd, returns
    ( is_freebsd ) || return 0

    log "Loading linux kernel module."
    # It's just it? -n make sure to not load again if it's already loaded.
    if ! kldstat -v | grep -E 'linux64(aout|elf)' > /dev/null; then
        ( kldload -n linux64 > /dev/null 2>&1 ) || (
            log ${ERROR} "Failed to load linux compatibility." && return 1
        )
    fi

    return 0
}

unload_linux_compat()
{
    # If not freebsd, returns
    ( is_freebsd ) || return 0

    log "Unloading linux compatibility module."

    ( kldunload linux64 > /dev/null 2>&1 ) || (
        log ${WARN} "Linux kernel module still up." && return 1
    )

    return 0
}

is_something_running()
{
    ( fstat | grep ${1} >> /dev/null && return 0 ) || return 1
}

print_whats_running()
{
    log ${INFO} "${YELLOW}Proccess running on this jail:${NC}"
    # TODO> save into var with scape chars (break line etc)
    fstat | grep ${1}
    echo
}

# Check if there is anything mounted or running for the jail's path
is_something_mounted()
{
    ( mount | grep ${1} >> /dev/null && return 0 ) || return 1
}

print_whats_mounted()
{
    log ${INFO} "${YELLOW}Jail's mountpoints found:${NC}"
    # TODO> save into var with scape chars (break line etc)
    mount | grep ${1}
    echo
}

mount_jail()
{
    JAIL=${JAILS_PATH}/${NAME}
    # If freebsd, loads linux kern module
    ( is_freebsd ) && ( load_linux_compat || return 1 )

    if ! [ "${NAME}" ] || ! [ -d "${JAIL}" ]; then
        log ${ERROR} "System name not found."
        echo
        print_help
        return 1
    fi

    # create ${JAIL}/${EXTERN_MNT} for mounting
    # TODO: maybe this is not the wanted dir. How to check it into fstab?
    if ! [ -d "${JAIL}/${EXTERN_MNT}" ]; then
        log "Creating ${EXTERN_MNT} at jail's root..."
        mkdir -p ${JAIL}${EXTERN_MNT}
    fi

    if ! ( mount -a -F ${FSTAB_PATH}/fstab.${NAME} ); then
        log ${ERROR} "Couldn't mount linux environment. Did you edit the fstab?"

        umount -a -F ${FSTAB_PATH}/fstab.${NAME}
        return 1
    fi

    if [ "$VERBOSE" = true ] ; then
        log "${YELLOW}Mounted devices:${NC}"
        mount | grep ${JAIL}
    fi

    return 0
}

umount_jail()
{
    JAIL=${JAILS_PATH}/${NAME}

    # Pre-check before try to unmount
    if ! ( is_something_mounted ${JAIL} ); then
        log ${INFO} "There is nothing mounted for this jail."
        return 0
    fi

    if ( is_something_running ${JAIL} ); then
        log ${WARN} "There is something running for this jail."
        log ${WARN} "Please, kill them and call umount again."

        print_whats_running ${JAIL}
        return 1
    fi

    if ! [ "${NAME}" ] || ! [ -d "${JAIL}" ]; then
        log ${ERROR} "System name not found."
        print_help
        return 1
    fi

    log "Unmounting jail: ${JAIL}"

    # Unmount all fstab file, same as mount
    # TODO: not umount when another instance is open
    #       it forces umount so another instance may break while running
    umount -a -F ${FSTAB_PATH}/fstab.${NAME}

    if ( is_something_mounted ${JAIL} ) ; then
        log ${ERROR} "${YELLOW}Could not unmount all linux environment.${NC} Please, take care of them."

        print_whats_mounted ${JAIL}
        echo

        return 1
    fi

    # If freebsd, unloads linux kern module
    ( is_freebsd ) && unload_linux_compat

    return 0
}

#-------------- main starts here ---------------

# We need to shift the first options, otherwise getopts wont work
# Get chroot name
[ -z "${1}" ] && print_help "You forgot to pass the chroot name..." && exit 1
NAME=${1}
shift

# Get action (main command)
[ -z "${1}" ] && print_help "You forgot to pass an action..." && exit 1
ACTION=${1}
shift

# If action is "run", we expect another command before options
if [ "${ACTION}" = "run" ]; then
    # Check next param again due shifting...
    [ -z "${1}" ] && print_help "You forgot the chroot's name...?" && exit 1

    CMD=${1}; shift
    VERBOSE=false
fi

# Ok, now we can get the other options...
while getopts 'xV:mus:v:p:' c
do
    case $c in
        x) set -x ;;
        V) VERBOSE=$OPTARG ;;
        # bootstrap options
        s) SUITE=$OPTARG;;
        v) VARIANT=$OPTARG ;;
        # exec options
        m) SHOULD_MOUNT=true ;;
        u) SHOULD_UMOUNT=true ;;
        p) PARAMS=$OPTARG ;;
    esac
done

if [ ! `id -u` = "0" ]; then
    log ${ERROR} "Only the chosen one can delegate these actions. Where is your su?"
    print_help
    exit 1
fi

JAIL=${JAILS_PATH}/${NAME}

case ${ACTION} in
    bootstrap)
        bootstrap
        break;
        ;;
    mount)
        mount_jail && log "${GREEN}Done.${NC} You can run \"chroot ${JAIL}\" now."
        break
        ;;
    unmount | umount)
        umount_jail
        break
        ;;
    list)
        log "List not available yet..."
        break
        ;;
    boot)
        ( is_something_mounted  ${JAIL} || mount_jail ) && {
            echo
            echo -e "${YELLOW}${NAME} mounted ${GREEN}---------------------------------------------------------${NC}"
            chroot ${JAIL} /bin/bash

            log "${GREEN}Exiting...${NC}"
            echo -e "${GREEN}-------------------------------------------------------------------------${NC}"
            echo

            ( umount_jail && log "${GREEN}Done!${NC}" ) ||\
                log ${WARN} "${YELLOW}Umount may have been failed. Please, check the mensages above.${NC}"
        }
        break;
        ;;
    run)
        # check if should mount
        if [ ${SHOULD_MOUNT} = true ]; then
            ( is_something_mounted  ${JAIL} || mount_jail ) || (
                log ${ERROR} "Chroot is not mounted."
                exit 1
            )
        fi

        # exec
        # TODO: Clear outputs. It is too verbose, hard to see what is happening.
        echo
        echo -e "${GREEN}Run output:${NC}"
        echo -e "${GREEN}--------------------------------------${NC}"
        chroot ${JAIL} /bin/bash -c "${CMD} ${PARAMS}"
        echo -e "${GREEN}--------------------------------------${NC}"
        echo

        if [ ${SHOULD_UMOUNT} = true ]; then
            ( umount_jail && log "${GREEN}Done!${NC}" ) ||\
                log ${WARN} "${YELLOW}Umount may have been failed. Please, check the mensages above.${NC}"
        fi
        ;;
    *)
        print_help "The action '${ACTION}' does not exist."
        ;;
esac

exit $?
