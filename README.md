# my.scripts

This is a project where I will save my custom sh scripts.

## [FreeBSD update script](my.system)

I made it mainly to study the FreeBSD package system. It's not very useful for "prodution", but it helps (me at least) to remember some things. 

## [Audio output helper](my.audio)

Toggles, lists and changes audio output. just a helper.

## [Chroot starter](my.chroot)

Starts a chroot environment... for now, it only mounts an already created environment from a given path with a given fstab file. I'm still implementing it, when the boostraping is complete I start to write a doc.